﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : BaseState
{
    private Alien m_Alien;
    float nearestDistance = 1000;
    Transform nearestShipSystem;

    public IdleState(Alien alien) : base(alien.gameObject)
    {
        m_Alien = alien;
    }

    public override Type Tick()
    {
        if (!m_Alien.Target)
        {
            if(ChooseNearestSystem())
                return typeof(GoToSystemState);
            else
                return typeof(ChaseTargetState);
        }
        else
        {
            return typeof(ChaseTargetState);
        }

    }

    private bool ChooseNearestSystem()
    {
        nearestDistance = 1000;
        nearestShipSystem = null;

        foreach (ShipSystem shipSystem in GameManager.Instance.ShipSystems)
        {
            if (shipSystem.CurrentLifePoints > 0)
            {
                if (Vector3.Distance(transform.position, shipSystem.transform.position) < nearestDistance)
                {
                    nearestDistance = Vector3.Distance(transform.position, shipSystem.transform.position);
                    nearestShipSystem = shipSystem.transform;
                }
                   
            }
        }

        if (!nearestShipSystem)
        {
            m_Alien.Target = GameManager.Instance.Player;
            return false;
        }

        m_Alien.SetSystemTarget(nearestShipSystem);

        return true;
        
    }
}
