﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackTargetState : BaseState
{

    private Alien m_Alien;

    public AttackTargetState(Alien alien) : base(alien.gameObject)
    {
        m_Alien = alien;
    }

    public override Type Tick()
    {
        if(m_Alien.Target && m_Alien.TargetInRange)
        {
            m_Alien.ShootCanons.ShootingPatternRoutine(m_Alien.Target);
            return null;
        }
        else if(m_Alien.Target && !m_Alien.TargetInRange)
        {
            m_Alien.Movement.ResumeMoving();
            return typeof(ChaseTargetState);
        }

        m_Alien.Movement.ResumeMoving();
        return typeof(IdleState);
       
    }
}
