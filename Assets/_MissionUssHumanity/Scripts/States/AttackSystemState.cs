﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackSystemState : BaseState
{

    private Alien m_Alien;

    public AttackSystemState(Alien alien) : base(alien.gameObject)
    {
        m_Alien = alien;
    }

    public override Type Tick()
    {

        if (m_Alien.SystemTarget && m_Alien.SystemTargetInRange)
        {
            m_Alien.ShootCanons.ShootingRoutine(m_Alien.SystemTarget);
            return null;
        }
        else if (m_Alien.SystemTarget && !m_Alien.SystemTargetInRange)
        {
            m_Alien.Movement.ResumeMoving();
            return typeof(GoToSystemState);
        }

        m_Alien.Movement.ResumeMoving();
        return typeof(IdleState);

    }
}
