﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToSystemState : BaseState
{

    private Alien m_Alien;

    public GoToSystemState(Alien alien) : base(alien.gameObject)
    {
        m_Alien = alien;
    }

    public override Type Tick()
    {
        if (m_Alien.Target)
        {
            return typeof(ChaseTargetState);
        }

        if (m_Alien.SystemTarget && m_Alien.SystemTargetInRange)
        {
            m_Alien.Movement.StopMoving();
            return typeof(AttackSystemState);
        }
        else if (m_Alien.SystemTarget && !m_Alien.SystemTargetInRange)
        {
            
            m_Alien.Movement.ChaseTarget(m_Alien.SystemTarget.position);

            return null;
        }

        return typeof(IdleState);
    }
}
