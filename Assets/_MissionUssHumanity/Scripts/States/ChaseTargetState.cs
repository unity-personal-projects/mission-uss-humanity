﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseTargetState : BaseState
{

    private Alien m_Alien;

    public ChaseTargetState(Alien alien) : base(alien.gameObject)
    {
        m_Alien = alien;
    }

    public override Type Tick()
    {
        if (m_Alien.Target  && m_Alien.TargetInRange)
        {
            m_Alien.Movement.StopMoving();
            return typeof(AttackTargetState);
        }
        else if(m_Alien.Target && !m_Alien.TargetInRange)
        {
            m_Alien.Movement.ChaseTarget(m_Alien.Target.position);

            return null;
        }

        if (m_Alien.SystemTarget)
        {
            return typeof(GoToSystemState);
        }

        return typeof(IdleState);

    }
}
