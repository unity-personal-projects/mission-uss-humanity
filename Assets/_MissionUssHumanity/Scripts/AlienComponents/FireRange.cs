﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class FireRange : MonoBehaviour
{
    public List<Transform> visibleTargets = new List<Transform>();

    public event Action OnTargetInRange = delegate { };
    public event Action OnTargetOutOfRange = delegate { };

    public event Action OnSystemInRange = delegate { };
    public event Action OnSystemOutOfRange = delegate { };

    [SerializeField]
    private Alien alien = null;

    private void OnTriggerEnter(Collider other)
    {
        if (!visibleTargets.Contains(other.transform) && (other.tag.Equals("Player") || other.tag.Equals("Turret")))
        {
            if (alien.Target)
            {
                if (alien.Target.Equals(other.transform))
                {
                    RaycastHit hit;

                    if (Physics.Raycast(transform.position, (other.transform.position - transform.position), out hit))
                    {
                        if (hit.transform.Equals(alien.Target) && !visibleTargets.Contains(hit.transform))
                        {
                            visibleTargets.Add(other.transform);
                            OnTargetInRange();
                        }
                    }
                }
            }


        }

        else if (!visibleTargets.Contains(other.transform) && other.tag.Equals("ShipSystem") && !alien.Target)
        {
            if (alien.SystemTarget != null && alien.SystemTarget.Equals(other.transform))
            {
                RaycastHit hit;

                if (Physics.Raycast(transform.position, (other.transform.position - transform.position), out hit))
                {
                    if (hit.transform.Equals(alien.SystemTarget) && !visibleTargets.Contains(hit.transform))
                    {
                        visibleTargets.Add(other.transform);
                        OnSystemInRange();
                    }
                }
            }

        }

    }

    private void OnTriggerStay(Collider other)
    {
        if ((other.tag.Equals("Player") || other.tag.Equals("Turret")) && !alien.TargetInRange)
        {
            RaycastHit hit;

            if (Physics.Raycast(transform.position, (other.transform.position - transform.position), out hit))
            {
                if (alien.Target)
                {
                    if (hit.transform.Equals(alien.Target) && !visibleTargets.Contains(hit.transform))
                    {
                        visibleTargets.Add(other.transform);
                        OnTargetInRange();
                    }
                }

            }
        }
        else if (other.tag.Equals("ShipSystem") && !alien.SystemTargetInRange && !alien.Target)
        {
            RaycastHit hit;

            if (Physics.Raycast(transform.position, (other.transform.position - transform.position), out hit))
            {
                if (hit.transform.Equals(alien.SystemTarget) && !visibleTargets.Contains(hit.transform))
                {
                    visibleTargets.Add(other.transform);
                    OnSystemInRange();
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("Player") || other.tag.Equals("Turret"))
        {
            visibleTargets.Remove(other.transform);
            if (other.transform.Equals(alien.Target))
                OnTargetOutOfRange();
        }
        else if (other.tag.Equals("ShipSystem"))
        {
            visibleTargets.Remove(other.transform);
            if (other.transform.Equals(alien.SystemTarget))
                OnSystemOutOfRange();
        }
    }
}
