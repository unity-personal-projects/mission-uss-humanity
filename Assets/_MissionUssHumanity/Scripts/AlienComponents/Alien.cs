﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SDD.Events;

public class Alien : DestroyableElement
{
    [SerializeField]
    private Transform target;
    public Transform Target { get { return target; } set { target = value; } }


    [SerializeField]
    private Transform systemTarget;
    public Transform SystemTarget { get { return systemTarget; } private set { systemTarget = value; } }

    public bool TargetInRange; //{ get; private set; }
    public bool SystemTargetInRange; //{ get; private set; }


    public FieldOfView FieldOfView { get; private set; }

    [SerializeField]
    private FireRange fireRange;
    public FireRange FireRange { get { return fireRange; } private set { fireRange = value; } }

    [SerializeField]
    private ShootCanons shootCanons;
    public ShootCanons ShootCanons { get { return shootCanons; } private set { shootCanons = value; } }
    public Movement Movement { get; private set; }


    protected override void Awake()
    {
        base.Awake();
        InitializeStateMachine();

        FieldOfView = GetComponent<FieldOfView>();
        Movement = GetComponent<Movement>();
        GetComponent<FieldOfView>().OnTargetSighted += OnTargetSighted;
        GetComponent<FieldOfView>().OnSystemSighted += OnSystemSighted;
        GetComponent<FieldOfView>().OnSystemLost += OnSystemLost;
        GetComponent<FieldOfView>().OnTargetLost += OnTargetLost;

        FireRange.OnTargetInRange += OnTargetInRange;
        FireRange.OnSystemInRange += OnSystemInRange;
        FireRange.OnTargetOutOfRange += OnTargetOutOfRange;
        FireRange.OnSystemOutOfRange += OnSystemOutOfRange;

        EventManager.Instance.AddListener<SystemDestroyedEvent>(OnSystemDestroyed);
        EventManager.Instance.AddListener<GameOverEvent>(OnGameOver);
        EventManager.Instance.AddListener<GameVictoryEvent>(OnGameVictory);
        EventManager.Instance.AddListener<GamePlayEvent>(OnGamePlay);
    }

    private void OnGameVictory(GameVictoryEvent e)
    {
        gameOver();
    }

    private void OnGameOver(GameOverEvent e)
    {
        gameOver();
    }

    private void gameOver()
    {
        EventManager.Instance.Raise(new AlienDieEvent() { alien = gameObject });
        Destroy(gameObject);
    }

    private void OnSystemDestroyed(SystemDestroyedEvent systemDestroyedEvent)
    {
        if (systemDestroyedEvent.system.transform.Equals(systemTarget))
        {
            systemTarget = null;
            SystemTargetInRange = false;
        }
    }

    private void OnSystemOutOfRange()
    {
        SystemTargetInRange = false;
    }

    private void OnTargetOutOfRange()
    {
        TargetInRange = false;
    }

    private void OnSystemInRange()
    {
        SystemTargetInRange = true;
    }

    private void OnTargetInRange()
    {
        TargetInRange = true;
    }

    private void InitializeStateMachine()
    {
        var states = new Dictionary<Type, BaseState>()
        {
            {typeof(IdleState), new IdleState(this)},
            {typeof(AttackSystemState), new AttackSystemState(this)},
            {typeof(AttackTargetState), new AttackTargetState(this)},
            {typeof(GoToSystemState), new GoToSystemState(this)},
            {typeof(ChaseTargetState), new ChaseTargetState(this)}
        };

        GetComponent<StateMachine>().SetStates(states);
    }

    private void OnSystemSighted()
    {
        SetSystemTarget(FieldOfView.visibleTargets[FieldOfView.visibleTargets.Count - 1].transform);
    }

    private void OnSystemLost()
    {
        SetSystemTarget(null);
    }

    private void OnTargetSighted()
    {
        SetTarget(FieldOfView.visibleTargets[FieldOfView.visibleTargets.Count - 1]);
    }

    private void OnTargetLost()
    {
        SetTarget(null);
    }

    public void SetTarget(Transform target)
    {
        Target = target;
    }
    public void SetSystemTarget(Transform target)
    {
        SystemTarget = target;
    }

    void OnCollisionEnter(Collision other)
    {
        Bullet bullet = other.gameObject.GetComponent<Bullet>();
        if (bullet != null && (bullet.BulletType == BulletType.PlayerBullet || bullet.BulletType == BulletType.TurretBullet))
        {
            SetLifePoints(-bullet.Damage);
            if (!IsAlive())
            {
                StartCoroutine(Die());
            }
        }
    }

    private IEnumerator Die()
    {
        EventManager.Instance.Raise(new AlienDieEvent() { alien = gameObject });
        Destroy(gameObject);
        yield return null;
    }


    private void OnGamePlay(GamePlayEvent e)
    {
        SetInitialLifePoints();
    }
    
    private void OnDestroy()
    {
        EventManager.Instance.RemoveListener<GamePlayEvent>(OnGamePlay);
        EventManager.Instance.RemoveListener<SystemDestroyedEvent>(OnSystemDestroyed);
        EventManager.Instance.RemoveListener<GameOverEvent>(OnGameOver);
        EventManager.Instance.RemoveListener<GameVictoryEvent>(OnGameVictory);
    }
}
