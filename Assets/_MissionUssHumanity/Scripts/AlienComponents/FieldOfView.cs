﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class FieldOfView : MonoBehaviour
{
    public List<Transform> visibleTargets = new List<Transform>();

    public event Action OnTargetSighted = delegate { };
    public event Action OnTargetLost = delegate { };

    public event Action OnSystemSighted = delegate { };
    public event Action OnSystemLost = delegate { };

    private void OnTriggerEnter(Collider other)
    {
        if (!visibleTargets.Contains(other.transform) && (other.tag.Equals("Player") || other.tag.Equals("Turret")))
        {
            visibleTargets.Add(other.transform);
            OnTargetSighted();
        }
        else if(!visibleTargets.Contains(other.transform) && other.tag.Equals("ShipSystem"))
        {
            visibleTargets.Add(other.transform);
            OnSystemSighted();
        }
            
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("Player") || other.tag.Equals("Turret"))
        {
            visibleTargets.Remove(other.transform);
            OnTargetLost();
        }
        else if (other.tag.Equals("ShipSystem"))
        {
            visibleTargets.Remove(other.transform);
            OnSystemLost();
        }
    }
}
