﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Movement : MonoBehaviour
{

    private NavMeshAgent m_navMeshAgent;

    // Start is called before the first frame update
    void Start()
    {
        m_navMeshAgent = GetComponent<NavMeshAgent>();
        //m_navMeshAgent.Warp(transform.position);
    }

    public void ChaseTarget(Vector3 targetPosition)
    {
        SetDestination(targetPosition);
    }

    private void SetDestination(Vector3 targetPosition)
    {
        m_navMeshAgent.SetDestination(targetPosition);
    }

    public void StopMoving()
    {
        m_navMeshAgent.isStopped = true;
    }

    public void ResumeMoving()
    {
        m_navMeshAgent.isStopped = false;
    }

    public bool DestinationReached()
    {
        // Check if we've reached the destination
        if (!m_navMeshAgent.pathPending)
        {
            if (m_navMeshAgent.remainingDistance <= m_navMeshAgent.stoppingDistance)
            {
                if (!m_navMeshAgent.hasPath || m_navMeshAgent.velocity.sqrMagnitude == 0f)
                {
                    return true;
                }
            }
        }

        return false;
    }




}
