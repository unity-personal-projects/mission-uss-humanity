﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SDD.Events;

#region MenuManager Events
public class EscapeButtonClickedEvent : SDD.Events.Event
{
}
public class PlayButtonClickedEvent : SDD.Events.Event
{
}
public class ResumeButtonClickedEvent : SDD.Events.Event
{
}
public class MainMenuButtonClickedEvent : SDD.Events.Event
{
}

public class QuitButtonClickedEvent : SDD.Events.Event
{
}
#endregion

#region GameManager Events
public class GameMenuEvent : SDD.Events.Event
{
}
public class GamePlayEvent : SDD.Events.Event
{
}
public class GamePauseEvent : SDD.Events.Event
{
}
public class GameResumeEvent : SDD.Events.Event
{
}
public class GameOverEvent : SDD.Events.Event
{
}
public class GameVictoryEvent : SDD.Events.Event
{
}
#endregion

#region Alien Events

public class SpawnAlienEvent : SDD.Events.Event
{
    public GameObject alien;
}

public class AlienDieEvent : SDD.Events.Event
{
    public GameObject alien;
}

#endregion

#region Turrets Events

public class HealTurretDestroyedEvent : SDD.Events.Event
{
    public GameObject turret;
}

public class AttackTurretDestroyedEvent : SDD.Events.Event
{
    public GameObject turret;
}

#endregion

#region Player Events

public class HealPlayerEvent : SDD.Events.Event
{
}

public class PlayerDead : SDD.Events.Event
{
}

#endregion

#region Systems Events

public class SystemDestroyedEvent : SDD.Events.Event
{
    public ShipSystem system;
}

public class SystemRepairedEvent : SDD.Events.Event
{
    public ShipSystem system;
}

public class TogglePlayerTurretEvent : SDD.Events.Event
{
    public bool turretActive;
}

#endregion
