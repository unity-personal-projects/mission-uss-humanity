﻿using UnityEngine;

public class USSCamera : MonoBehaviour
{
    [SerializeField] private GameObject Player = null;

    void Update()
    {
        transform.position = Player.transform.position + new Vector3(0.0f, 10, 0.0f);
    }
}
