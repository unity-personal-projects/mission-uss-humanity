﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SDD.Events;
using System;

[RequireComponent(typeof(Rigidbody))]
public class Player : DestroyableElement
{
    [SerializeField] private GameObject m_AttackTurretPrefab = null;
    [SerializeField] private GameObject m_HealTurretPrefab = null;
    [SerializeField] private float m_Speed = 200;
    [SerializeField] private float m_HealRange = 4f;
    [SerializeField] private float m_HealRate = 10f;
    [SerializeField] private int m_MaxAttackTurretsAmount = 2;
    [SerializeField] private GameObject m_ImageHealTurret = null;
    [SerializeField] private List<GameObject> m_CrossesImages = null;
    [SerializeField] private List<GameObject> m_AttackTurretsImages = null;
    [SerializeField] private GameObject m_BulletPrefab = null;
    [SerializeField] private float m_Cooldown = 0.2f;
    [SerializeField] private float m_CooldownLastTurretPlacement = 0.15f;

    Vector3 m_InitialPosition = Vector3.zero;
    Quaternion m_InitialRotation = Quaternion.identity;
    private bool m_IsPlacingTurret = false;
    private bool m_IsPlacingHealTurret = false;
    private bool m_IsRotatingTurret = false;
    private float m_TurretPlacementRange = 3.5f;
    private GameObject m_Turret = null;
    private List<GameObject> m_AttackTurrets = new List<GameObject>();
    private List<GameObject> m_HealTurrets = new List<GameObject>();
    private Rigidbody m_Rigidbody = null;
    private Quaternion m_Rotation = Quaternion.identity;
    private Vector3 m_Movement = Vector3.zero;
    public bool IsPlacingTurret { get => m_IsPlacingTurret; }
    public bool IsRotatingTurret { get => m_IsRotatingTurret; }
    private Transform m_BulletAnchor = null;
    private Coroutine m_Shooting = null;
    private float m_TimeLastTurretPlacement = 0;

    protected override void Awake()
    {
        base.Awake();
        m_InitialPosition = transform.position;
        m_InitialRotation = transform.rotation;
        m_Rigidbody = GetComponent<Rigidbody>();
        m_BulletAnchor = GetComponentInChildren<PlayerBulletAnchorSelector>().transform;
        EventManager.Instance.AddListener<HealTurretDestroyedEvent>(OnHealTurretDestroyed);
        EventManager.Instance.AddListener<AttackTurretDestroyedEvent>(OnAttackTurretDestroyed);
        EventManager.Instance.AddListener<HealPlayerEvent>(OnHealPlayer);
        EventManager.Instance.AddListener<GamePlayEvent>(OnGamePlay);
        EventManager.Instance.AddListener<GameOverEvent>(OnGameOver);
        EventManager.Instance.AddListener<GameVictoryEvent>(OnGameVictory);
        EventManager.Instance.AddListener<GameMenuEvent>(OnGameMenu);
    }

    private void Update()
    {
        if (!GameManager.Instance.IsPlaying)
        {
            return;
        }
        TurretShaderManagement();
        ShootingManagement();
        PlaceTurret();
        PlaceHealTurret();
        Heal();
        PickupTurret();
        DefenseSystemDestroyTurrets();
        ImagesToggle();
    }

    private void TurretShaderManagement()
    {
        if (m_IsPlacingTurret || m_IsRotatingTurret || m_IsPlacingHealTurret)
        {
            m_TimeLastTurretPlacement = Time.time;
            Shader.SetGlobalFloat("_IsPlayerPlacing", 1);
        }
        else
        {
            Shader.SetGlobalFloat("_IsPlayerPlacing", 0);
        }
        Shader.SetGlobalVector("_PlayerPosition", transform.position);
        Shader.SetGlobalFloat("_PlayerPlacementMaxRange", m_TurretPlacementRange);
        Shader.SetGlobalFloat("_PlayerPlacementMinRange", 1.2f);
    }
    private void ShootingManagement()
    {
        if (Input.GetMouseButton(0)
            && m_Shooting == null
            && !m_IsPlacingTurret
            && !m_IsRotatingTurret
            && !m_IsPlacingHealTurret
            && m_TimeLastTurretPlacement + m_CooldownLastTurretPlacement < Time.time)
        {
            m_Shooting = StartCoroutine(Shooting());
        }
    }
    private IEnumerator Shooting()
    {
        Instantiate(m_BulletPrefab, m_BulletAnchor.transform.position, m_BulletAnchor.transform.rotation);
        yield return new WaitForSeconds(m_Cooldown);
        m_Shooting = null;
    }

    private void PlaceTurret()
    {
        if (Input.GetKeyDown(KeyCode.A)
            && !m_IsPlacingTurret
            && !m_IsRotatingTurret
            && !m_IsPlacingHealTurret
            && GameManager.Instance.DefenseSystemActive
            && m_AttackTurrets.Count < m_MaxAttackTurretsAmount)
        {
            m_IsPlacingTurret = true;
            m_Turret = Instantiate(m_AttackTurretPrefab);
        }
        if (m_IsPlacingTurret)
        {
            Ray ray;
            RaycastHit hit;

            int layerMask = LayerMask.GetMask("Ground");

            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
            {
                float distance = Vector3.Distance(transform.position, hit.point);
                if (distance > m_TurretPlacementRange)
                {
                    m_Turret.transform.position = transform.position + (hit.point - transform.position).normalized * m_TurretPlacementRange;
                }
                else if (distance > 1.2)
                {
                    m_Turret.transform.position = hit.point;
                }
                else
                {
                    m_Turret.transform.position = transform.position + (hit.point - transform.position).normalized * 1.2f;
                }
                if (Input.GetMouseButtonDown(0))
                {
                    m_IsPlacingTurret = false;
                    m_IsRotatingTurret = true;
                    return;
                }

                if (Input.GetMouseButtonDown(1))
                {
                    Destroy(m_Turret);
                    m_IsPlacingTurret = false;
                }
            }
        }

        if (m_IsRotatingTurret && !m_IsPlacingTurret)
        {
            Vector3 mouse = Input.mousePosition;
            Vector3 mouseWorld = Camera.main.ScreenToWorldPoint(new Vector3(mouse.x, mouse.y, 10));
            mouseWorld.y = m_Turret.transform.position.y;
            m_Turret.transform.LookAt(mouseWorld);

            if (Input.GetMouseButtonDown(0))
            {
                m_AttackTurrets.Add(m_Turret);
                m_IsRotatingTurret = false;
                m_Turret.GetComponent<Collider>().enabled = true;
                Turret turret = m_Turret.GetComponent<Turret>();
                turret.Enable();
            }
            if (Input.GetMouseButtonDown(1))
            {
                Destroy(m_Turret);
                m_IsRotatingTurret = false;
            }
        }
    }

    private void Heal()
    {
        if (Input.GetKey(KeyCode.R))
        {
            foreach (ShipSystem system in GameManager.Instance.ShipSystems)
            {
                float distance = Vector3.Distance(transform.position, system.transform.position);
                if (distance <= m_HealRange)
                {
                    system.SetLifePoints(m_HealRate * Time.deltaTime);
                }
            }
        }
    }

    private void OnHealPlayer(HealPlayerEvent e)
    {
        SetLifePoints(100);
    }

    private void PlaceHealTurret()
    {
        if (Input.GetKeyDown(KeyCode.E) && m_HealTurrets.Count < 1 && !m_IsPlacingHealTurret && !m_IsPlacingTurret && !m_IsRotatingTurret)
        {
            m_IsPlacingHealTurret = true;
            m_Turret = Instantiate(m_HealTurretPrefab);
        }

        if (m_IsPlacingHealTurret)
        {
            Ray ray;
            RaycastHit hit;

            int layerMask = LayerMask.GetMask("Ground");

            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
            {
                float distance = Vector3.Distance(transform.position, hit.point);
                if (distance > m_TurretPlacementRange)
                {
                    m_Turret.transform.position = transform.position + (hit.point - transform.position).normalized * m_TurretPlacementRange;

                }
                else if (distance > 1.2)
                {
                    m_Turret.transform.position = hit.point;
                }
                else
                {
                    m_Turret.transform.position = transform.position + (hit.point - transform.position).normalized * 1.2f;
                }
                if (Input.GetMouseButtonDown(0))
                {
                    m_Turret.GetComponent<Collider>().enabled = true;
                    m_HealTurrets.Add(m_Turret);
                    m_Turret = null;
                    m_IsPlacingHealTurret = false;
                    return;
                }

                if (Input.GetMouseButtonDown(1))
                {
                    Destroy(m_Turret);
                    m_IsPlacingHealTurret = false;
                }
            }
        }
    }

    private void DefenseSystemDestroyTurrets()
    {
        if (!GameManager.Instance.DefenseSystemActive && m_AttackTurrets.Count > 0)
        {
            m_IsPlacingTurret = false;
            m_IsRotatingTurret = false;
            foreach (GameObject turret in m_AttackTurrets)
            {
                Destroy(turret);
            }
            m_AttackTurrets = new List<GameObject>();
            m_Turret = null;
        }
    }

    private void OnHealTurretDestroyed(HealTurretDestroyedEvent e)
    {
        m_IsPlacingHealTurret = false;
        m_HealTurrets.Remove(e.turret);
        m_Turret = null;
    }

    private void OnAttackTurretDestroyed(AttackTurretDestroyedEvent e)
    {
        m_IsPlacingTurret = false;
        m_IsRotatingTurret = false;
        m_AttackTurrets.Remove(e.turret);
        m_Turret = null;
    }

    private void PickupTurret()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            Ray ray;
            RaycastHit hit;
            int layerMask = LayerMask.GetMask("Turret");
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
            {
                m_Turret = hit.collider.gameObject;
                m_Turret.GetComponent<Collider>().enabled = false;
                if (m_Turret.GetComponent<HealTurret>() != null)
                {
                    m_IsPlacingHealTurret = false;
                    m_HealTurrets.Remove(m_Turret);
                }
                else
                {
                    m_IsPlacingTurret = false;
                    m_IsRotatingTurret = false;
                    m_AttackTurrets.Remove(m_Turret);
                }
                Destroy(m_Turret);
            }
        }
    }

    private void FixedUpdate()
    {
        if (!GameManager.Instance.IsPlaying)
        {
            return;
        }
        Vector3 mouse = Input.mousePosition;
        Vector3 mouseWorld = Camera.main.ScreenToWorldPoint(new Vector3(mouse.x, mouse.y, 10));
        mouseWorld.y = transform.position.y;
        m_Movement = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
        m_Rigidbody.velocity = m_Movement * m_Speed;
        transform.LookAt(mouseWorld);
    }

    private void OnCollisionEnter(Collision other)
    {
        Bullet bullet = other.gameObject.GetComponent<Bullet>();
        if (bullet != null && bullet.BulletType == BulletType.AlienBullet)
        {
            SetLifePoints(-bullet.Damage);
            if (CurrentLifePoints <= 0)
            {
                EventManager.Instance.Raise(new PlayerDead());
            }
        }
    }
    private void ImagesToggle()
    {
        m_ImageHealTurret.SetActive(m_HealTurrets.Count == 0);
        m_AttackTurretsImages[1].SetActive(m_AttackTurrets.Count == 0);
        m_AttackTurretsImages[0].SetActive(m_AttackTurrets.Count <= 1);
        m_CrossesImages[1].SetActive(!GameManager.Instance.DefenseSystemActive);
        m_CrossesImages[0].SetActive(!GameManager.Instance.DefenseSystemActive);
    }


    private void OnGamePlay(GamePlayEvent e)
    {
        ResetPlayer();
    }

    private void OnGameVictory(GameVictoryEvent e)
    {
        ResetPlayer();
    }

    private void OnGameOver(GameOverEvent e)
    {
        ResetPlayer();
    }

    private void OnGameMenu(GameMenuEvent e)
    {
        ResetPlayer();
    }

    private void ResetPlayer()
    {
        transform.position = m_InitialPosition;
        transform.rotation = m_InitialRotation;
        m_Rigidbody.MovePosition(transform.position);
        m_Rigidbody.velocity = Vector3.zero;
        m_Rigidbody.angularVelocity = Vector3.zero;
        SetInitialLifePoints();
    }

    private void OnDestroy()
    {
        EventManager.Instance.RemoveListener<GamePlayEvent>(OnGamePlay);
        EventManager.Instance.RemoveListener<HealTurretDestroyedEvent>(OnHealTurretDestroyed);
        EventManager.Instance.RemoveListener<AttackTurretDestroyedEvent>(OnAttackTurretDestroyed);
        EventManager.Instance.RemoveListener<HealPlayerEvent>(OnHealPlayer);
        EventManager.Instance.RemoveListener<GameOverEvent>(OnGameOver);
        EventManager.Instance.RemoveListener<GameVictoryEvent>(OnGameVictory);
        EventManager.Instance.RemoveListener<GameMenuEvent>(OnGameMenu);
    }
}
