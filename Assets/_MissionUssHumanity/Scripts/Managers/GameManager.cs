﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SDD.Events;
using System;

public enum GameState
{
    gameMenu,
    gamePlay,
    gamePause,
    gameOver,
    gameVictory
}
public class GameManager : Manager<GameManager>
{
    [SerializeField] private float m_EarthCountdown = 600f;
    [SerializeField] private float m_OxygenTank = 100f;
    [SerializeField] private float m_OxygenTankDecreaseRate = 4f;
    [SerializeField] private float m_OxygenTankIncreaseRate = 5f;
    [SerializeField] private bool m_IsLifeSupportActive = true;
    [SerializeField] Text m_EarthCountdownText = null;

    private GameState m_GameState;
    public bool IsPlaying
    {
        get
        {
            return m_GameState == GameState.gamePlay;
        }
    }

    [SerializeField] private float m_InitialOxygenTank = 0f;
    private float m_EarthInitialCountdown = 0f;
    List<ShipSystem> m_ShipSystems = new List<ShipSystem>();
    private List<GameObject> m_Aliens = new List<GameObject>();
    private bool m_DefenseSystemActive = true;
    Coroutine m_CountdownCoroutine = null;
    private Transform m_Player;
    public Transform Player { get => m_Player; }
    public List<GameObject> Aliens { get => m_Aliens; }
    public List<ShipSystem> ShipSystems { get => m_ShipSystems; }
    public float EarthCountdown { get => m_EarthCountdown; }
    public bool DefenseSystemActive { get => m_DefenseSystemActive; }

    public override void SubscribeEvents()
    {
        base.SubscribeEvents();
        // Game playing Events
        EventManager.Instance.AddListener<SystemRepairedEvent>(OnSystemRepaired);
        EventManager.Instance.AddListener<SystemDestroyedEvent>(OnSystemDestroyed);
        EventManager.Instance.AddListener<SpawnAlienEvent>(OnSpawnAlien);
        EventManager.Instance.AddListener<AlienDieEvent>(OnAlienDie);

        // MainMenuEvents
        EventManager.Instance.AddListener<MainMenuButtonClickedEvent>(OnMainMenuButtonClicked);
        EventManager.Instance.AddListener<PlayButtonClickedEvent>(OnPlayButtonClicked);
        EventManager.Instance.AddListener<ResumeButtonClickedEvent>(OnResumeButtonClicked);
        EventManager.Instance.AddListener<EscapeButtonClickedEvent>(OnEscapeButtonClicked);
        EventManager.Instance.AddListener<QuitButtonClickedEvent>(OnQuitButtonClicked);

        //EndGame
        EventManager.Instance.AddListener<PlayerDead>(OnPlayerDead);
    }

    protected override void Awake()
    {
        base.Awake();

        m_ShipSystems.AddRange(FindObjectsOfType<ShipSystem>());
        m_EarthCountdownText = FindObjectOfType<EarthCountdownUI>().GetComponent<Text>();
        StartCoroutine(UpdateCountdown());
        m_EarthInitialCountdown = EarthCountdown;
        m_InitialOxygenTank = m_OxygenTank;
        m_Player = FindObjectOfType<Player>().transform;
    }

    protected override IEnumerator InitCoroutine()
    {
        Menu();
        InitNewGame();
        yield break;
    }

    #region Time
    void SetTimeScale(float newTimeScale)
    {
        Time.timeScale = newTimeScale;
    }
    #endregion

    void InitNewGame()
    {
        m_EarthCountdown = m_EarthInitialCountdown;
        m_OxygenTank = m_InitialOxygenTank;
    }

    private void OnSystemRepaired(SystemRepairedEvent e)
    {
        switch (e.system.Type)
        {
            case SystemType.Defense:
                m_DefenseSystemActive = true;
                break;
            case SystemType.Hull:
                EventManager.Instance.Raise(new TogglePlayerTurretEvent() { turretActive = true });
                break;
            case SystemType.LifeSupport:
                m_IsLifeSupportActive = true;
                EventManager.Instance.Raise(new HealPlayerEvent());
                break;
            case SystemType.Propulsion:
                if (m_CountdownCoroutine == null)
                {
                    m_CountdownCoroutine = StartCoroutine(StartCountdown());
                }
                break;
        }
    }

    private void OnSystemDestroyed(SystemDestroyedEvent e)
    {
        switch (e.system.Type)
        {
            case SystemType.Defense:
                m_DefenseSystemActive = false;
                break;
            case SystemType.Hull:
                EventManager.Instance.Raise(new TogglePlayerTurretEvent() { turretActive = true });
                break;
            case SystemType.LifeSupport:
                m_IsLifeSupportActive = false;
                break;
            case SystemType.Propulsion:
                if (m_CountdownCoroutine != null)
                {
                    StopCoroutine(m_CountdownCoroutine);
                    m_CountdownCoroutine = null;
                }
                break;
        }
    }

    private IEnumerator StartCountdown()
    {
        while (m_EarthCountdown > 0)
        {
            m_EarthCountdown -= Time.deltaTime;
            yield return null;
        }
        m_EarthCountdown = 0;
    }

    private IEnumerator LifeSupport()
    {
        while (m_OxygenTank > 0)
        {
            if (m_IsLifeSupportActive)
            {
                m_OxygenTank += m_OxygenTankIncreaseRate * Time.deltaTime;
                m_OxygenTank = m_OxygenTank > 100 ? 0 : m_OxygenTank;
            }
            else
            {
                m_OxygenTank -= m_OxygenTankDecreaseRate * Time.deltaTime;
                m_OxygenTank = m_OxygenTank < 0 ? 0 : m_OxygenTank;
            }
            yield return null;
        }
    }

    private void OnSpawnAlien(SpawnAlienEvent e)
    {
        m_Aliens.Add(e.alien);
    }

    private void OnAlienDie(AlienDieEvent e)
    {
        m_Aliens.Remove(e.alien);
    }

    private IEnumerator UpdateCountdown()
    {
        while (m_EarthCountdown > 0)
        {
            int minutes = Convert.ToInt32(Math.Truncate(m_EarthCountdown / 60f));
            int seconds = Convert.ToInt32(m_EarthCountdown % 60f);
            string minText = (minutes < 10 ? "0" + minutes.ToString() : minutes.ToString());
            string secText = (seconds < 10 ? "0" + seconds.ToString() : seconds.ToString());
            m_EarthCountdownText.text = minText + ":" + (secText == "60" ? "59" : secText);
            yield return null;
        }
        m_EarthCountdownText.text = "00:00";
        EventManager.Instance.Raise(new GameVictoryEvent());
    }

    #region Callbacks to Events issued by MenuManager

    private void OnMainMenuButtonClicked(MainMenuButtonClickedEvent e)
    {
        Menu();
    }

    private void OnPlayButtonClicked(PlayButtonClickedEvent e)
    {

        Play();
    }

    private void OnResumeButtonClicked(ResumeButtonClickedEvent e)
    {
        Resume();
    }

    private void OnEscapeButtonClicked(EscapeButtonClickedEvent e)
    {
        if (IsPlaying)
        {
            Pause();
        }
    }

    private void OnQuitButtonClicked(QuitButtonClickedEvent e)
    {
        Application.Quit();
    }

    #endregion

    #region GameState Methods

    private void Menu()
    {
        SetTimeScale(0);
        m_GameState = GameState.gameMenu;
        EventManager.Instance.Raise(new GameMenuEvent());
    }

    private void Play()
    {
        InitNewGame();
        SetTimeScale(1);
        m_GameState = GameState.gamePlay;
        EventManager.Instance.Raise(new GamePlayEvent());
    }

    private void Pause()
    {
        if (!IsPlaying)
        {
            return;
        }
        SetTimeScale(0);
        m_GameState = GameState.gamePause;
        EventManager.Instance.Raise(new GamePauseEvent());
    }

    private void Resume()
    {
        if (IsPlaying)
        {
            return;
        }

        SetTimeScale(1);
        m_GameState = GameState.gamePlay;
        EventManager.Instance.Raise(new GameResumeEvent());
    }

    private void Over()
    {
        SetTimeScale(0);
        m_GameState = GameState.gameOver;
        m_Aliens = new List<GameObject>();
        EventManager.Instance.Raise(new GameOverEvent());
    }

    private void OnPlayerDead(PlayerDead e)
    {
        Over();
    }
    protected override void GameVictory(GameVictoryEvent e)
    {
        Over();
    }
    #endregion

    public override void UnsubscribeEvents()
    {
        base.UnsubscribeEvents();
        // Game playing Events
        EventManager.Instance.RemoveListener<SystemRepairedEvent>(OnSystemRepaired);
        EventManager.Instance.RemoveListener<SystemDestroyedEvent>(OnSystemDestroyed);
        EventManager.Instance.RemoveListener<SpawnAlienEvent>(OnSpawnAlien);
        EventManager.Instance.RemoveListener<AlienDieEvent>(OnAlienDie);

        // MainMenuEvents
        EventManager.Instance.RemoveListener<MainMenuButtonClickedEvent>(OnMainMenuButtonClicked);
        EventManager.Instance.RemoveListener<PlayButtonClickedEvent>(OnPlayButtonClicked);
        EventManager.Instance.RemoveListener<ResumeButtonClickedEvent>(OnResumeButtonClicked);
        EventManager.Instance.RemoveListener<EscapeButtonClickedEvent>(OnEscapeButtonClicked);
        EventManager.Instance.RemoveListener<QuitButtonClickedEvent>(OnQuitButtonClicked);

        //EndGame
        EventManager.Instance.AddListener<PlayerDead>(OnPlayerDead);
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
    }
}
