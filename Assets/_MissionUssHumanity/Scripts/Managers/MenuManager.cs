using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SDD.Events;
using UnityEngine.UI;

public class MenuManager : Manager<MenuManager>
{

    [Header("MenuManager")]

    #region Panels
    [Header("Panels")]
    [SerializeField] GameObject m_PanelMainMenu = null;
    [SerializeField] GameObject m_PanelInGameMenu = null;
    [SerializeField] GameObject m_PanelHUD = null;
    [SerializeField] GameObject m_PanelGameOver = null;
    [SerializeField] GameObject m_PanelHowToPlay = null;
    [SerializeField] GameObject m_PanelSettings = null;

    List<GameObject> m_AllPanels;
    #endregion

    #region Events' subscription
    public override void SubscribeEvents()
    {
        base.SubscribeEvents();
        EventManager.Instance.AddListener<PlayerDead>(OnPlayerDead);
    }

    public override void UnsubscribeEvents()
    {
        base.UnsubscribeEvents();
    }
    #endregion

    #region Monobehaviour lifecycle
    protected override void Awake()
    {
        base.Awake();
        RegisterPanels();
    }
    protected override IEnumerator Start()
    {
        base.Start();
        m_PanelInGameMenu.SetActive(false);
        m_PanelHUD.SetActive(false);
        m_PanelGameOver.SetActive(false);
        m_PanelHowToPlay.SetActive(false);
        m_PanelSettings.SetActive(false);
        yield return null;
    }

    private void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            EscapeButtonHasBeenClicked();
        }
    }
    #endregion

    #region Panel Methods
    void RegisterPanels()
    {
        m_AllPanels = new List<GameObject>()
        {
            m_PanelMainMenu,
            m_PanelInGameMenu,
            m_PanelGameOver,
            m_PanelHowToPlay,
            m_PanelSettings
        };
    }

    void OpenPanel(GameObject panel)
    {
        foreach (var item in m_AllPanels)
        {
            if (item)
            {
                item.SetActive(item == panel);
            }
        }
    }
    #endregion

    #region UI OnClick Events
    public void EscapeButtonHasBeenClicked()
    {
        EventManager.Instance.Raise(new EscapeButtonClickedEvent());
    }

    public void PlayButtonHasBeenClicked()
    {
        EventManager.Instance.Raise(new PlayButtonClickedEvent());
    }

    public void ResumeButtonHasBeenClicked()
    {
        EventManager.Instance.Raise(new ResumeButtonClickedEvent());
    }

    public void MainMenuButtonHasBeenClicked()
    {
        EventManager.Instance.Raise(new MainMenuButtonClickedEvent());
    }

    public void QuitButtonHasBeenClicked()
    {
        EventManager.Instance.Raise(new QuitButtonClickedEvent());
    }
    public void HowToPlayButtonHasBeenClicked()
    {
        m_PanelMainMenu.SetActive(false);
        m_PanelHowToPlay.SetActive(true);
    }
    public void SettingsButtonHasBeenClicked()
    {
        m_PanelMainMenu.SetActive(false);
        m_PanelSettings.SetActive(true);
    }

    #endregion

    #region Callbacks to GameManager events
    protected override void GameMenu(GameMenuEvent e)
    {
        OpenPanel(m_PanelMainMenu);
    }

    protected override void GamePlay(GamePlayEvent e)
    {
        m_PanelHUD.SetActive(true);
        OpenPanel(m_PanelHUD);
    }

    protected override void GamePause(GamePauseEvent e)
    {
        m_PanelHUD.SetActive(false);
        OpenPanel(m_PanelInGameMenu);
    }

    protected override void GameResume(GameResumeEvent e)
    {
        m_PanelHUD.SetActive(true);
        OpenPanel(m_PanelHUD);
    }

    protected override void GameOver(GameOverEvent e)
    {
        OpenPanel(m_PanelGameOver);
    }

    protected override void GameVictory(GameVictoryEvent e)
    {
        OpenPanel(m_PanelGameOver);
    }
    #endregion

    private void OnPlayerDead(PlayerDead e)
    {
        m_PanelMainMenu.SetActive(false);
        m_PanelInGameMenu.SetActive(false);
        m_PanelHUD.SetActive(false);
        m_PanelGameOver.SetActive(true);
    }

    protected override IEnumerator InitCoroutine()
    {
        throw new NotImplementedException();
    }
}