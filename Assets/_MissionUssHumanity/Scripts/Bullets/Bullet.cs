﻿using UnityEngine;
using SDD.Events;

public enum BulletType
{
    AlienBullet,
    PlayerBullet,
    TurretBullet
}

public class Bullet : MonoBehaviour
{
    [SerializeField] private BulletType m_BulletType = BulletType.PlayerBullet;
    [SerializeField] private float m_Damage = 5f;
    [SerializeField] private float m_Speed = 3f;
    [SerializeField] private GameObject particles = null;

    public BulletType BulletType { get => m_BulletType; }

    public float Damage { get => m_Damage; }

    void Awake()
    {
        EventManager.Instance.AddListener<GameOverEvent>(OnGameOver);
        EventManager.Instance.AddListener<GameVictoryEvent>(OnGameVictory);
    }

    private void OnGameVictory(GameVictoryEvent e)
    {
        gameOver();
    }

    private void OnGameOver(GameOverEvent e)
    {
        gameOver();
    }

    private void gameOver()
    {
        Destroy(gameObject);
    }

    private void Start()
    {
        Destroy(gameObject, 6f);
        GetComponent<Rigidbody>().velocity = transform.forward * m_Speed;

        SfxManager.Instance.PlaySfx2D("LaserShoot");
    }
    private void OnCollisionEnter(Collision other)
    {
        Destroy(gameObject);
        SfxManager.Instance.PlaySfx2D("LaserHit");
    }

    private void OnDestroy()
    {
        EventManager.Instance.RemoveListener<GameOverEvent>(OnGameOver);
        EventManager.Instance.RemoveListener<GameVictoryEvent>(OnGameVictory);
        Instantiate(particles, transform.position, Quaternion.identity, null);
    }
}