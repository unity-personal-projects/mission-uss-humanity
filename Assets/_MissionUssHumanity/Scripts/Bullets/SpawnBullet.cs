﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBullet : MonoBehaviour
{
    [SerializeField] private GameObject m_Bullet = null;

    private IEnumerator Start()
    {
        while (true)
        {
            GameObject g = Instantiate(m_Bullet);
            g.transform.position = transform.position;
            g.transform.rotation = transform.rotation;
            g.GetComponent<Rigidbody>().velocity = -Vector3.forward * 5;
            yield return new WaitForSeconds(1.5f);
        }
    }

}
