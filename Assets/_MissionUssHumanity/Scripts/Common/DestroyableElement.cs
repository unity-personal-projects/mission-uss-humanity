﻿using UnityEngine.UI;
using UnityEngine;

public class DestroyableElement : MonoBehaviour
{
    [SerializeField] private float maxLifePoints = 100;
    [SerializeField] protected float m_CurrentLifePoints = 0;
    [SerializeField] protected Image m_CurrentLifeBar = null;

    private float m_InitialCurrentLifePoints = 0f;

    public float CurrentLifePoints
    {
        get => m_CurrentLifePoints;
    }
    public float MaxLifePoints { get => maxLifePoints; }

    protected virtual void Awake()
    {
        m_InitialCurrentLifePoints = m_CurrentLifePoints;
    }

    public virtual void SetLifePoints(float value)
    {
        m_CurrentLifePoints += value;
        m_CurrentLifePoints = Mathf.Clamp(CurrentLifePoints, 0, MaxLifePoints);
        UpdateLifeBar();
    }

    public virtual void SetInitialLifePoints()
    {
        m_CurrentLifePoints = m_InitialCurrentLifePoints;
        m_CurrentLifePoints = Mathf.Clamp(CurrentLifePoints, 0, MaxLifePoints);
        UpdateLifeBar();
    }

    protected bool IsAlive()
    {
        return CurrentLifePoints > 0f;
    }

    protected void UpdateLifeBar()
    {
        m_CurrentLifeBar.fillAmount = m_CurrentLifePoints / MaxLifePoints;
    }

}
