﻿using UnityEngine;

public class TurretPlayerManager : MonoBehaviour
{
    [SerializeField] private GameObject m_TurretPrefab = null;

    private bool m_IsPlacingTurret = false;
    private bool m_IsRotatingTurret = false;

    private GameObject m_Turret = null;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A) && !m_IsPlacingTurret && !m_IsRotatingTurret)
        {
            m_IsPlacingTurret = true;
            m_Turret = Instantiate(m_TurretPrefab);
        }

        if (m_IsPlacingTurret)
        {
            Ray ray;
            RaycastHit hit;
            int layerMask = ~LayerMask.GetMask("Turret");

            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
            {
                m_Turret.transform.position = hit.point;

                if (Input.GetMouseButtonDown(0))
                {
                    m_IsPlacingTurret = false;
                    m_IsRotatingTurret = true;
                    return;
                }
                if (Input.GetMouseButtonDown(1))
                {
                    Destroy(m_Turret);
                    m_IsPlacingTurret = false;
                }
            }
        }

        if (m_IsRotatingTurret && !m_IsPlacingTurret)
        {
            Vector3 mouse = Input.mousePosition;
            Vector3 mouseWorld = Camera.main.ScreenToWorldPoint(new Vector3(mouse.x, mouse.y, 10));
            mouseWorld.y = m_Turret.transform.position.y;
            m_Turret.transform.LookAt(mouseWorld);

            if (Input.GetMouseButtonDown(0))
            {
                m_IsRotatingTurret = false;
                Turret turret = m_Turret.GetComponent<Turret>();
                turret.Enable();
            }
            if (Input.GetMouseButtonDown(1))
            {
                Destroy(m_Turret);
                m_IsRotatingTurret = false;
            }
        }
    }
}
