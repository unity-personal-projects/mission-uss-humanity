﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretBullet : MonoBehaviour
{
    [SerializeField]
    private float m_Speed = 1;

    public void SetTargetDirection(Vector3 targetDirection)
    {
        transform.forward = targetDirection.normalized;
    }

    private void Update()
    {
        transform.position += transform.forward * m_Speed * Time.deltaTime;
    }

    private void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);
    }
}
