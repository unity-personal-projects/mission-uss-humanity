﻿using UnityEngine;
using SDD.Events;

public class HealTurret : DestroyableElement
{
    [SerializeField] private float m_HealSpeed = 5f;

    public float HealSpeed { get => m_HealSpeed; }

    protected override void Awake()
    {
        base.Awake();
        EventManager.Instance.AddListener<GameOverEvent>(OnGameOver);
        EventManager.Instance.AddListener<GameVictoryEvent>(OnGameVictory);
    }
    void OnCollisionEnter(Collision other)
    {
        Bullet bullet = other.gameObject.GetComponent<Bullet>();
        if (bullet != null && bullet.BulletType == BulletType.AlienBullet)
        {
            SetLifePoints(-bullet.Damage);
        }
    }
    private void Update()
    {
        if (!IsAlive())
        {
            EventManager.Instance.Raise(new HealTurretDestroyedEvent() { turret = gameObject });
            Destroy(gameObject);
        }
    }

    private void OnGameVictory(GameVictoryEvent e)
    {
        GameOver();
    }

    private void OnGameOver(GameOverEvent e)
    {
        GameOver();
    }

    private void GameOver()
    {
        Destroy(gameObject);
    }

    void OnDestroy()
    {
        EventManager.Instance.RemoveListener<GameOverEvent>(OnGameOver);
        EventManager.Instance.RemoveListener<GameVictoryEvent>(OnGameVictory);
    }
}
