﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootCanons : MonoBehaviour
{
    [SerializeField] private GameObject[] m_Canons = null;
    [SerializeField] private GameObject m_MuzzlePattern = null;
    [SerializeField] private float m_RateOfFire = 1;
    [SerializeField] private float m_Cooldown = 0f;
    [SerializeField] private GameObject m_Bullet = null;

    [SerializeField] private float m_RotationSpeed = 10f;
    [SerializeField] private int m_NbBulletsPattern = 4;
    [SerializeField] private int m_PatternSpread = 10;

    private Vector3 m_direction;
    private Quaternion m_lookRotation;
    public int PatternType = 0;

    public void ShootingRoutine(Transform target)
    {

        //find the vector pointing from our position to the target
        m_direction = (target.position - transform.position).normalized;

        //create the rotation we need to be in to look at the target
        m_lookRotation = Quaternion.LookRotation(m_direction);

        //rotate us over time according to speed until we are in the required rotation
        transform.rotation = Quaternion.Slerp(transform.rotation, m_lookRotation, Time.deltaTime * m_RotationSpeed);

        if (m_Cooldown <= 0)
        {
            Fire();
        }
        else
        {
            m_Cooldown -= Time.deltaTime;
        }


    }

    public void ShootingPatternRoutine(Transform target)
    {
        if (PatternType == 1)
        {
            m_direction = (target.position - transform.position).normalized;
            m_lookRotation = Quaternion.LookRotation(m_direction);
            transform.rotation = Quaternion.Slerp(transform.rotation, m_lookRotation, Time.deltaTime * m_RotationSpeed);

            if (m_Cooldown <= 0)
            {
                StartCoroutine(FirePatternCoroutine());
            }
            else
            {
                m_Cooldown -= Time.deltaTime;
            }
        }
        else
        {

            transform.Rotate(new Vector3(0, 30, 0) * Time.deltaTime * m_RotationSpeed);

            if (m_Cooldown <= 0)
            {
                FirePattern();
            }
            else
            {
                m_Cooldown -= Time.deltaTime * 10;
            }
        }



    }

    private void Fire()
    {
        foreach (GameObject gameObject in m_Canons)
        {
            Instantiate(m_Bullet, gameObject.transform.position, gameObject.transform.rotation, null);
        }

        m_Cooldown = m_RateOfFire;
    }

    private IEnumerator FirePatternCoroutine()
    {
        Quaternion rotation = transform.rotation;

        Instantiate(m_Bullet, m_MuzzlePattern.transform.position, rotation, null);

        for (int i = 0; i < m_NbBulletsPattern / 2; i++)
        {
            rotation *= Quaternion.Euler(0, - m_PatternSpread, 0);
            Instantiate(m_Bullet, m_MuzzlePattern.transform.position, rotation, null);
        }
         rotation = transform.rotation;
        for (int j = 0; j < m_NbBulletsPattern /2; j++)
        {
            rotation *= Quaternion.Euler(0,  m_PatternSpread, 0);
            Instantiate(m_Bullet, m_MuzzlePattern.transform.position, rotation, null);
        }


        m_Cooldown = m_RateOfFire;

        yield return null;
    }

    private void FirePattern()
    {

        Instantiate(m_Bullet, m_MuzzlePattern.transform.position, transform.rotation, null);


        m_Cooldown = m_RateOfFire;
    }
}
