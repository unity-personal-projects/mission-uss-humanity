﻿using System;
using System.Collections;
using System.Collections.Generic;
using SDD.Events;
using UnityEngine;

public class Turret : DestroyableElement
{

    [SerializeField]
    private float m_VisionAngle = 30;
    [SerializeField]
    private float m_Range = 5;
    [SerializeField]
    private float m_RotationSpeed = 1;
    [SerializeField]
    private float m_FireSpeed = 1;

    [SerializeField]
    private GameObject m_BulletPrefab = null;
    [SerializeField]
    private Transform m_BulletStartPosition = null;

    private Vector3 m_InitialForward = Vector3.zero;
    private float m_FireCooldown = 0;

    private bool m_Enabled = false;

    protected override void Awake()
    {
        base.Awake();
        UpdateLifeBar();
        EventManager.Instance.AddListener<GameOverEvent>(OnGameOver);
        EventManager.Instance.AddListener<GameVictoryEvent>(OnGameVictory);
    }

    public void Enable()
    {
        m_InitialForward = transform.forward;
        this.m_Enabled = true;
    }

    private void Update()
    {
        if (!m_Enabled)
        {
            return;
        }

        if (m_FireCooldown > 0)
        {
            m_FireCooldown -= Time.deltaTime;
        }

        GameObject closestEnemy = null;
        float closestRange = Mathf.Infinity;

        foreach (GameObject enemy in GameManager.Instance.Aliens)
        {
            float distanceFromAlien = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceFromAlien <= m_Range && distanceFromAlien < closestRange)
            {
                closestRange = distanceFromAlien;
                closestEnemy = enemy;
            }
        }
        if (closestEnemy != null)
        {
            Vector3 direction = closestEnemy.transform.position - transform.position;
            float angleFromInitialForward = Mathf.Rad2Deg * Mathf.Acos(Vector3.Dot(m_InitialForward, direction.normalized));
            if (angleFromInitialForward <= m_VisionAngle)
            {
                RotateTowards(closestEnemy.transform);
                float angleFromForward = Mathf.Rad2Deg * Mathf.Acos(Vector3.Dot(transform.forward, direction.normalized));
                if (angleFromForward <= 0.1 && m_FireCooldown <= 0)
                {
                    m_FireCooldown = m_FireSpeed;
                    Fire(closestEnemy.transform);
                }
            }
        }
    }

    private void RotateTowards(Transform target)
    {
        Quaternion endRotation = Quaternion.FromToRotation(transform.forward, target.position - transform.position) * transform.rotation;
        float step = m_RotationSpeed * Time.deltaTime;
        transform.rotation = Quaternion.RotateTowards(transform.rotation, endRotation, m_RotationSpeed);
    }

    private void Fire(Transform target)
    {
        GameObject g = Instantiate(m_BulletPrefab, m_BulletStartPosition.position, m_BulletStartPosition.rotation);
    }

    void OnCollisionEnter(Collision other)
    {
        Bullet bullet = other.gameObject.GetComponent<Bullet>();
        if (bullet != null && bullet.BulletType == BulletType.AlienBullet)
        {
            SetLifePoints(-bullet.Damage);
            if (!IsAlive())
            {
                EventManager.Instance.Raise(new AttackTurretDestroyedEvent() { turret = gameObject });
                Destroy(gameObject);
            }
        }
    }

    private void OnGameVictory(GameVictoryEvent e)
    {
        GameOver();
    }

    private void OnGameOver(GameOverEvent e)
    {
        GameOver();
    }

    private void GameOver()
    {
        Destroy(gameObject);
    }

    void OnDestroy()
    {
        EventManager.Instance.RemoveListener<GameOverEvent>(OnGameOver);
        EventManager.Instance.RemoveListener<GameVictoryEvent>(OnGameVictory);
    }
}
