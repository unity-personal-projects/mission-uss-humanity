﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SDD.Events;

public class HullSpawner : MonoBehaviour
{
    [SerializeField] private List<GameObject> m_AliensPrefab = null;
    [SerializeField] private Transform m_AlienSpawnPoint = null;
    [SerializeField] private int m_InfectionProbability = 30;
    [SerializeField] private float m_SpawnCooldown = 15;
    private float m_StartSpawnCooldown = 0;

    private float m_PreviousTimeSpawn = 0f;
    private float m_StartCountdown = 0;

    private void Awake()
    {
        m_StartSpawnCooldown = m_SpawnCooldown;
    }

    private void Start()
    {
        m_StartCountdown = GameManager.Instance.EarthCountdown;
    }

    private void Update()
    {
        m_SpawnCooldown = m_SpawnCooldown <= 5 ? 5 : m_StartSpawnCooldown * (GameManager.Instance.EarthCountdown / m_StartCountdown);
        if (m_PreviousTimeSpawn >= m_SpawnCooldown)
        {
            if (UnityEngine.Random.Range(0, 101) > m_InfectionProbability && GameManager.Instance.Aliens.Count < 6)
            {
                int randomAlien = UnityEngine.Random.Range(0, m_AliensPrefab.Count);
                GameObject alien = Instantiate(m_AliensPrefab[randomAlien], m_AlienSpawnPoint.position, m_AlienSpawnPoint.rotation);
                EventManager.Instance.Raise(new SpawnAlienEvent() { alien = alien });
            }
            m_PreviousTimeSpawn = 0;
        }
        m_PreviousTimeSpawn += Time.deltaTime;
    }
}