﻿using UnityEngine;
using SDD.Events;
using System.Collections;
using System;

public enum SystemType
{
    Propulsion = 0,
    Defense = 1,
    Hull = 2,
    LifeSupport = 3
}

public class ShipSystem : DestroyableElement
{
    [SerializeField] private SystemType type = SystemType.Defense;

    private GameObject m_DestroyedMesh = null;
    private GameObject m_RepairedMesh = null;
    private HealTurret m_HealTurret = null;

    public SystemType Type { get => type; }

    public override void SetLifePoints(float value)
    {
        bool wasRepaired = m_CurrentLifePoints > 0;
        base.SetLifePoints(value);
        if (wasRepaired && !IsAlive())
        {
            StartCoroutine(DestroySystem());
            EventManager.Instance.Raise(new SystemDestroyedEvent() { system = this });
        }
        if (!wasRepaired && IsAlive())
        {
            StartCoroutine(RepairSystem());
            EventManager.Instance.Raise(new SystemRepairedEvent() { system = this });
        }
    }

    public override void SetInitialLifePoints()
    {
        base.SetInitialLifePoints();
        if (IsAlive())
        {
            StartCoroutine(RepairSystem());
            EventManager.Instance.Raise(new SystemRepairedEvent() { system = this });
        }
        else
        {
            StartCoroutine(DestroySystem());
            EventManager.Instance.Raise(new SystemDestroyedEvent() { system = this });
        }
    }

    protected override void Awake()
    {
        base.Awake();
        m_DestroyedMesh = GetComponentInChildren<DestroyedSelector>(true).gameObject;
        m_RepairedMesh = GetComponentInChildren<RepairedSelector>(true).gameObject;

        if (type == SystemType.Hull)
        {
            StartCoroutine(CreateAlienBreaches());
        }
        EventManager.Instance.AddListener<GamePlayEvent>(OnGamePlay);
    }

    private void OnCollisionEnter(Collision other)
    {
        Bullet bullet = other.gameObject.GetComponent<Bullet>();
        if (bullet != null && bullet.BulletType == BulletType.AlienBullet)
        {
            SetLifePoints(-bullet.Damage);
            if (m_HealTurret != null)
            {
                m_HealTurret.SetLifePoints(-bullet.Damage);
            }
        }
    }

    private void OnCollisionStay(Collision other)
    {
        m_HealTurret = other.gameObject.GetComponent<HealTurret>();

        if (m_HealTurret != null)
        {
            if (CurrentLifePoints < MaxLifePoints)
            {
                SetLifePoints(m_HealTurret.HealSpeed * Time.deltaTime);
                SfxManager.Instance.PlaySfx2D("Heal");
            }
            else
            {
                SfxManager.Instance.PlaySfx2D("Succes");
            }

        }
    }

    private void OnCollisionExit(Collision other)
    {
        if (other.gameObject.GetComponent<HealTurret>())
        {
            m_HealTurret = null;
        }
    }

    private IEnumerator DestroySystem()
    {
        m_RepairedMesh.SetActive(false);
        yield return null;
        m_DestroyedMesh.SetActive(true);
    }

    private IEnumerator RepairSystem()
    {
        m_DestroyedMesh.SetActive(false);
        yield return null;
        m_RepairedMesh.SetActive(true);
    }

    private IEnumerator CreateAlienBreaches()
    {
        while (true)
        {
            if (IsAlive())
            {
                float delay = UnityEngine.Random.Range(45f, 85f);
                yield return new WaitForSeconds(delay);
                SetLifePoints(-100f);
            }
            yield return null;
        }
    }

    private void OnGamePlay(GamePlayEvent e)
    {
        SetInitialLifePoints();
    }

    private void OnDestroy()
    {
        EventManager.Instance.RemoveListener<GamePlayEvent>(OnGamePlay);
    }
}
